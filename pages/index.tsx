import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'index', 'footer'])),
    },
  };
}

export default function Home() {
  const { t } = useTranslation('index');

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
        previewPath=''
      />
      <div className="text-center divide-y divide-gray-200 dark:divide-gray-700">
        <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-300">
          {t('header')}
        </h1>
      </div>

      <div className="pt-6 text-center dark:text-gray-200">
        {t('description')}
      </div>
    </>
  );
}
