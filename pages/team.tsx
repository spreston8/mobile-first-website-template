import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'team',
        'footer',
        'copyButton',
      ])),
    },
  };
}

export default function Team() {
  const { t } = useTranslation('team');

  return (
    <>
      <PageSeo
        title="Team"
        description="Team"
        url={`${siteMetadata.siteUrl}/team`}
        previewPath=''
      />
      <div className="divide-y">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <h1 className="text-3xl font-extrabold tracking-tight text-gray-900 leading-9 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
            {t('team')}
          </h1>
        </div>
      </div>
    </>
  );
}
