import { PageSeo } from '@components/SEO';
import siteMetadata from '@data/siteMetadata';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'about', 'footer'])),
    },
  };
}

export default function About() {
  const { t } = useTranslation('about');

  return (
    <>
      <PageSeo
        title="About"
        description="About Us"
        url={`${siteMetadata.siteUrl}/about`}
        previewPath=''
      />

      <div className="pt-6 pb-8 space-y-2 md:space-y-5">
        <h1 className="text-3xl font-extrabold tracking-tight text-gray-900 leading-9 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
          {t('header')}
        </h1>
        <h2 className='pt-10'>
          {t('about')}
        </h2>
      </div>
    </>
  );
}
