const siteMetadata = {
  title: 'M-FWT',
  author: 'Stephen Preston',
  headerTitle: 'Testing',
  description:
    'Mobile-First Website Template',
  siteUrl: 'https://gitlab.com/spreston8/mobile-first-website-template',
  siteRepo: 'https://gitlab.com/spreston8/mobile-first-website-template',
  email: 'stevepreston747@gmail.com',
  gitlab: 'https://gitlab.com/spreston8',
  twitter: 'https://twitter.com',
  linkedin: 'https://www.linkedin.com/in/spreston8/',

  locales: [{ en: 'English' }, { fr: 'français' }],
  language: 'English',
};

export default siteMetadata;