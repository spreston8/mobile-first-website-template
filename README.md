# Mobile-First Website Template

Boilerplate for creating mobile-first websites. With the increase use of mobile phones and in-app browsing, mobile-first websites aim at providing the best experience for users on their phones. This template provides a good start for developing such websites.

## Features

- Minimalistic and clean design for phone and computer browsing
- Light/Dark mode toggle
- Multiple language capability (Engligh and French are default)
- Home page along with team and about pages
- Social Icons

## Using 
- React
- NextJs
- Typescript 
- TailwindCSS
- Webpack