/**
 * @type {import('next').NextConfig}
 */

const { i18n } = require('./next-i18next.config');
const nextConfig = {
  i18n,
  // reactStrictMode: true,
  // https://github.com/vercel/next.js/discussions/37614
  trailingSlash: true,
  pageExtensions: ['js', 'jsx', 'md', 'mdx', 'ts', 'tsx'],
  // staticPageGenerationTimeout: 300,
  webpack: (config, { dev, isServer }) => {
    config.module.rules.push(
      {
        test: /\.(png|jpe?g|gif|mp4)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      }
    );

    return config;
  },
};

module.exports = nextConfig;
