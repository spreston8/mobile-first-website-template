import headerNavLinks from '@data/headerNavLinks';
import Logo from '../public/static/img/logo.svg';
import SectionContainer from './SectionContainer';
import Footer from './Footer';
import MobileNav from './MobileNav';
import ThemeSwitch from './ThemeSwitch';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';

type HeaderLinks = {
  title: string;
  href: string;
};

function LayoutWrapper({ children }) {
  const { t } = useTranslation('common');

  return (
    <SectionContainer>
      <div className="flex flex-col">
        <header className="flex items-center justify-between py-4 sm:py-10">
          <Link href="/">
            <Logo className="w-20" />
          </Link>
          <div className="flex items-center text-base">
            <div className="hidden sm:flex items-center">
              {headerNavLinks.map((link: HeaderLinks) => (
                <Link
                  key={link.title}
                  href={link.href}
                  className="font-medium text-[#5F5F5F] sm:p-4 dark:text-gray-100 hover:text-blue-400 dark:hover:text-blue-400"
                >
                  {t(`headerNavLinks.${link.title}`)}
                </Link>
              ))}
            </div>
            <div className="flex">
              <ThemeSwitch />
            </div>
            <div className="pt-1">
              <MobileNav />
            </div>
          </div>
        </header>
        <main className="mb-auto">{children}</main>
        <Footer />
      </div>
    </SectionContainer>
  );
}

export default LayoutWrapper;
