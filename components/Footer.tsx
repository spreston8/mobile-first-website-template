import siteMetadata from '@data/siteMetadata';
import SocialIcon from '@components/social-icons';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import SelectLocale from './SelectLocale';

export default function Footer() {
  const { t } = useTranslation('footer');

  return (
    <footer className='absolute inset-x-0 bottom-2'>
      <div className="flex flex-col items-center">
        <div className="flex mb-3 space-x-4">
          <SocialIcon
            kind="mail"
            href={`mailto:${siteMetadata.email}`}
            size={6}
          />
          <SocialIcon kind="gitlab" href={siteMetadata.gitlab} size={6} />
          <SocialIcon kind="linkedin" href={siteMetadata.linkedin} size={6} />
          <SocialIcon kind="twitter" href={siteMetadata.twitter} size={6} />
        </div>
        <div className="flex mb-2">
        <SelectLocale />
        </div>
        <div className="mb-2 text-sm text-gray-500 dark:text-gray-400">
          <div>
            {t('footer')}{' '}
            <Link href="https://tailwindcss.com/" target='_blank' rel="noopener noreferrer">TailwindCSS,</Link>
            <Link href="https://nextjs.org/" target='_blank' rel="noopener noreferrer"> NextJS</Link> &{' '}
            <Link href="https://reactjs.org/" target='_blank' rel="noopener noreferrer">React</Link>
          </div>
        </div>
      </div>
    </footer>
  );
}
