import { useRouter } from 'next/router';

export default function useRouterContext() {
  const router = useRouter();
  return router;
}
